import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { storeConfig } from './middleware/storeConfig';
import App from './appContainer';


const initialState = {};

const store = storeConfig(initialState);

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
document.getElementById('universal-node-summit-app'));
