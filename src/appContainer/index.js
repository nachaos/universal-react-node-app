import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as AppActions from './actions';
import { Header } from "../components/header";
import { Navbar } from "../components/navbar";
import { Posts } from "../components/posts";

class App extends Component {

  componentDidMount() {
    this.props.fetchPostsData();
  }

  render() {
    const { postsData, todosData, selectedTodo, selectedPost } = this.props;

    return (
      <main>
        <Header/>
        <Navbar/>
        {this.props.postsData && <Posts postsData={postsData} selectedPost={selectedPost}/>}
      </main>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    postsData: state.appReducers.postsData,
    todosData: state.appReducers.todosData,
    selectedTodo: state.appReducers.selectedTodo,
    selectedPost: state.appReducers.selectedPost
  }
};

export default connect(mapStateToProps, AppActions)(App);