import fetch from 'isomorphic-fetch';

// Actions Types in same file for convenience
export const RECEIVE_POSTS_DATA = 'RECEIVE_POSTS_DATA';
export const SELECT_POST = 'SELECT_POST';


function receivePostsData(payload) {
  return {
    type: RECEIVE_POSTS_DATA,
    payload
  }
}

export async function fetchPostsData() {
  const data = await fetch('https://jsonplaceholder.typicode.com/posts');
  const payload = await data.json();

  return receivePostsData(payload);
}

export function selectPost(postId) {
  return {
    type: SELECT_POST,
    postId
  }
}