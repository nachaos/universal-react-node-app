import React from 'react';
import './style.css';

export const Navbar = () => {
  return (
    <nav>
      <ul>
        <li>Home</li>
        <li>Posts</li>
        <li>Todos</li>
      </ul>
    </nav>
  )
};