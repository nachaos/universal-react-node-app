import React, { Component } from 'react';

export class Posts extends Component {


  renderPostItems() {
    return this.props.postsData.map((data, index) => {

      return (
        <div key={index} className="body" id={`post-id-${data.id}`}>
          <h5 className="title">{data.title}</h5>
          <span className="body">{data.body}</span>
        </div>
      )
    });
  }

  render() {
    return (
      <div className="posts-list">
        {this.renderPostItems()}
      </div>
    )
  }
}