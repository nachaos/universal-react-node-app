import React from 'react';
import './style.css';

export const Header = () => {
  return (
    <header>
      <h3>
        Welcome to your unstyled universal Posts & Todo Site! Super Useful :-)
      </h3>
    </header>
  )
};